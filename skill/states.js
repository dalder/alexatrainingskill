'use strict';

exports.register = function register(skill) {
  skill.onIntent('LaunchIntent', () => ({ reply: 'Intent.Launch', to: 'LaunchJokeTellingState' }));

  skill.onIntent('AMAZON.HelpIntent', () => ({ reply: 'Intent.Help', to: 'die' }));

  //this is not being used
  skill.onIntent('RandomJokeIntent', () => ({ reply: 'Intent.RandomJoke.Main', to: 'LaunchJokeTellingState' }));

  skill.onState('LaunchJokeTellingState', (request) => {
    if (request.intent.name === 'AMAZON.YesIntent') {
        return { reply: 'Intent.RandomJoke.Main', to: 'LaunchJokeTellingState' };
    } else if (request.intent.name === 'AMAZON.NoIntent') {
      return { reply: 'Intent.RandomJoke.No', to: 'die' };
    }
  });
};