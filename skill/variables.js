'use strict';

const requestPromise = require('request-promise');

exports.RandomJoke = function RandomJoke(model) {
  let options = {
      uri: `https://api.chucknorris.io/jokes/random`,
      headers: {
          'User-Agent': 'Request-Promise'
      },
      json: true,
      model: model
  };
  return requestPromise(options)
      .then(function (res) {
      var joke = res.value;
      return joke.toString();
      })
      .catch(function (err) {
        // @todo add "try again" support for this session
        return 'Sorry. Something went wrong';
      });
};
