'use strict';

const views = (function views() {
  return {
    Intent: {
      Launch: {
          ask: 'would you like to hear a chuck norris joke?'
      },
      Help: {
        tell: 'Say, tell me a joke'
      },
      RandomJoke: {
        Main:{
          ask: '{RandomJoke} ...Would you like to hear another?'
        },
        No:{
          tell: 'okay, bye.'
        }
      }
    },
  };
}());
module.exports = views;